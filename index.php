<?php

require_once 'app/Core/Core.php';

require_once 'lib/Database/Connection.php';

require_once 'app/Controller/HomeController.php';
require_once 'app/Controller/ErroController.php';
require_once 'app/Controller/CategoriaController.php';
require_once 'app/Controller/ProdutoController.php';

require_once 'app/Model/Produto.php';
require_once 'app/Model/Categoria.php';

require_once 'vendor/autoload.php';




$template = file_get_contents('app/View/Template/layout.html');

ob_start();
	$core = new Core;
	$core->start($_GET);

	$saida = ob_get_contents();
ob_end_clean();

$tplPronto = str_replace('{{area_dinamica}}', $saida, $template);
echo $tplPronto;