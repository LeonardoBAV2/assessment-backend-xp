<?php

class Produto
{

	public static function load()
	{
		$con = Connection::getConn();

		$sql = "SELECT * FROM produtos ORDER BY id DESC";
		$sql = $con->prepare($sql);
		$sql->execute();

		$resultado = array();

		while ($row = $sql->fetchObject('Produto')) {
			$row->categorias = Categoria::loadByProduto($row->id);
			$resultado[] = $row;
		}

		/*if (!$resultado) {
			throw new Exception("Não foi encontrado nenhum registro no banco");
		}*/

		return $resultado;
	}

	public static function loadById($idPost)
	{
		$con = Connection::getConn();

		$sql = "SELECT * FROM produtos WHERE id = :id";
		$sql = $con->prepare($sql);
		$sql->bindValue(':id', $idPost, PDO::PARAM_INT);
		$sql->execute();

		$resultado = $sql->fetchObject('Produto');
		$resultado->categorias = Categoria::loadByProduto($resultado->id);

		if (!$resultado) {
			throw new Exception("Não foi encontrado nenhum registro no banco");
		}

		return $resultado;
	}

	public static function create($dadosPost)
	{
		$con = Connection::getConn();

		$sql = $con->prepare('INSERT INTO produtos (nome, sku, preco, quantidade, descricao) VALUES (:nome, :sku, :preco, :quantidade, :descricao)');
		$sql->bindValue(':nome', $dadosPost['nome']);
		$sql->bindValue(':sku', $dadosPost['sku']);
		$sql->bindValue(':preco', $dadosPost['preco']);
		$sql->bindValue(':quantidade', $dadosPost['quantidade']);
		$sql->bindValue(':descricao', $dadosPost['descricao']);
		$res = $sql->execute();
		$id = $con->lastInsertId();
		if ($res == 0) {
			throw new Exception("Falha ao inserir produto");
			return false;
		}
		return $id;
	}

	public static function saveCategorias($produto_id, $categorias)
	{
		$con = Connection::getConn();

		$sql = $con->prepare('DELETE FROM produto_categoria WHERE produto_id = :produto_id');
		$sql->bindValue(':produto_id', $produto_id);
		$res = $sql->execute();
		foreach ($categorias as $id) {
			$sql = $con->prepare('INSERT INTO produto_categoria (produto_id, categoria_id) VALUES (:produto_id, :categoria_id)');
			$sql->bindValue(':produto_id', $produto_id);
			$sql->bindValue(':categoria_id', $id);
			$res = $sql->execute();
		}
	}



	public static function update($params)
	{
		$con = Connection::getConn();

		$sql = "UPDATE produtos SET nome = :nome, sku = :sku, preco = :preco, quantidade = :quantidade, descricao = :descricao WHERE id = :id";
		$sql = $con->prepare($sql);
		$sql->bindValue(':nome', $params['nome']);
		$sql->bindValue(':sku', $params['sku']);
		$sql->bindValue(':preco', $params['preco']);
		$sql->bindValue(':quantidade', $params['quantidade']);
		$sql->bindValue(':descricao', $params['descricao']);
		$sql->bindValue(':id', $params['id']);
		$resultado = $sql->execute();

		if ($resultado == 0) {
			throw new Exception("Falha ao alterar produto");

			return false;
		}

		return true;
	}

	public static function delete($id)
	{
		$con = Connection::getConn();

		$sql = "DELETE FROM produtos WHERE id = :id";
		$sql = $con->prepare($sql);
		$sql->bindValue(':id', $id);
		$resultado = $sql->execute();

		if ($resultado == 0) {
			throw new Exception("Falha ao deletar produto");

			return false;
		}

		return true;
	}
}
