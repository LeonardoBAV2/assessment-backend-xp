<?php

	class Categoria
	{

		public static function load()
		{
			$con = Connection::getConn();

			$sql = "SELECT * FROM categorias ORDER BY id DESC";
			$sql = $con->prepare($sql);
			$sql->execute();

			$resultado = array();

			while ($row = $sql->fetchObject('Categoria')) {
				$resultado[] = $row;
			}

			/*if (!$resultado) {
				throw new Exception("Não foi encontrado nenhum registro no banco");		
			}*/

			return $resultado;
        }

        public static function loadById($idPost)
		{
			$con = Connection::getConn();

			$sql = "SELECT * FROM categorias WHERE id = :id";
			$sql = $con->prepare($sql);
			$sql->bindValue(':id', $idPost, PDO::PARAM_INT);
			$sql->execute();

			$resultado = $sql->fetchObject('Categoria');

			if (!$resultado) {
				throw new Exception("Não foi encontrado nenhum registro no banco");	
			} 

			return $resultado;
        }
        
        public static function loadByProduto($id)
		{
			$con = Connection::getConn();

			$sql = "SELECT categorias.id, categorias.codigo, categorias.nome FROM `categorias`
            LEFT JOIN produto_categoria
            ON categorias.id = produto_categoria.categoria_id
            where produto_categoria.produto_id = :id";

			$sql = $con->prepare($sql);
			$sql->bindValue(':id', $id, PDO::PARAM_INT);
            $sql->execute();
            
            $resultado = array();
            while ($row = $sql->fetchObject('Categoria')) {
				$resultado[] = $row;
			}

			return $resultado;
		}


        
        public static function create($dadosPost)
		{
			$con = Connection::getConn();

			$sql = $con->prepare('INSERT INTO categorias (codigo, nome) VALUES (:codigo, :nome)');
			$sql->bindValue(':codigo', $dadosPost['codigo']);
			$sql->bindValue(':nome', $dadosPost['nome']);
            $res = $sql->execute();
            
			if ($res == 0) {
				throw new Exception("Falha ao inserir publicação");
				return false;
			}
			return true;
        }

        public static function update($params)
		{
			$con = Connection::getConn();

			$sql = "UPDATE categorias SET nome = :nome, codigo = :codigo WHERE id = :id";
			$sql = $con->prepare($sql);
			$sql->bindValue(':nome', $params['nome']);
			$sql->bindValue(':codigo', $params['codigo']);
			$sql->bindValue(':id', $params['id']);
			$resultado = $sql->execute();

			if ($resultado == 0) {
				throw new Exception("Falha ao alterar categoria");

				return false;
			}

			return true;
		}
        
        public static function delete($id)
		{
			$con = Connection::getConn();

			$sql = "DELETE FROM categorias WHERE id = :id";
			$sql = $con->prepare($sql);
			$sql->bindValue(':id', $id);
			$resultado = $sql->execute();

			if ($resultado == 0) {
				throw new Exception("Falha ao deletar categoria");

				return false;
			}

			return true;
		}

		

	}
