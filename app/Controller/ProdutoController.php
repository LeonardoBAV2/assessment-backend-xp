<?php

class ProdutoController
{
    public function index($params)
    {
        try {
            $produtos = Produto::load();

            $loader = new \Twig\Loader\FilesystemLoader('app/View');
            $twig = new \Twig\Environment($loader);
            $template = $twig->load('products.html');

            $parametros = array();
            $parametros['produtos'] = $produtos;

            $conteudo = $template->render($parametros);
            echo $conteudo;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function create()
    {
        try {
            $loader = new \Twig\Loader\FilesystemLoader('app/View');
            $twig = new \Twig\Environment($loader);
            $template = $twig->load('addProduct.html');

            $categorias = Categoria::load();
            $parametros = array();
            $parametros['categorias'] = $categorias;

            $conteudo = $template->render($parametros);
            echo $conteudo;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function store()
    {
        try {
            $produto_id = Produto::create($_POST);
            Produto::saveCategorias($produto_id, $_POST['categorias']);

            echo '<script>alert("Produto inserido com sucesso!");</script>';
            echo '<script>location.href="?pagina=produto&metodo=index"</script>';
        } catch (Exception $e) {
            echo '<script>alert("' . $e->getMessage() . '");</script>';
            echo '<script>location.href="?pagina=produto&metodo=create"</script>';
        }
    }

    public function edit($paramId)
    {
        try {
            $loader = new \Twig\Loader\FilesystemLoader('app/View');
            $twig = new \Twig\Environment($loader);
            $template = $twig->load('editProduct.html');

            $produto = Produto::loadById($paramId);

            $parametros = array();
            $parametros['id'] = $produto->id;
            $parametros['nome'] = $produto->nome;
            $parametros['sku'] = $produto->sku;
            $parametros['preco'] = $produto->preco;
            $parametros['quantidade'] = $produto->quantidade;
            $parametros['descricao'] = $produto->descricao;
            $parametros['categorias_selected'] = $produto->categorias;

            $categorias = Categoria::load();
            $parametros['categorias'] = $categorias;


            $conteudo = $template->render($parametros);
            echo $conteudo;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function update()
	{
		try {
            Produto::update($_POST);
            Produto::saveCategorias($_POST['id'], $_POST['categorias']);

			echo '<script>alert("Produto alterado com sucesso!");</script>';
			echo '<script>location.href="?pagina=produto&metodo=index"</script>';
		} catch (Exception $e) {
			echo '<script>alert("' . $e->getMessage() . '");</script>';
			echo '<script>location.href="?pagina=produto&metodo=edit&id='.$_POST['id'].'"</script>';
		}
	}

    public function destroy($paramId)
    {
        try {
            Produto::delete($paramId);

            echo '<script>alert("Produto deletado com sucesso!");</script>';
            echo '<script>location.href="?pagina=produto&metodo=index"</script>';
        } catch (Exception $e) {
            echo '<script>alert("' . $e->getMessage() . '");</script>';
            echo '<script>location.href="?pagina=produto&metodo=index"</script>';
        }
    }
}
