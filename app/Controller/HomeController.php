<?php

	class HomeController
	{
		public function index()
		{
			try {
				$produtos = Produto::load();

				$loader = new \Twig\Loader\FilesystemLoader('app/View');
				$twig = new \Twig\Environment($loader);
				$template = $twig->load('dashboard.html');

				$parametros = array();
				$parametros['produtos'] = $produtos;
				//var_dump($colecPostagens);

				$conteudo = $template->render($parametros);
				echo $conteudo;

				
				
			} catch (Exception $e) {
				echo $e->getMessage();
			}
			
		}
	}
