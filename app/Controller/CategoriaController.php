<?php

class CategoriaController
{
    public function index($params)
    {
        try {
            $categorias = Categoria::load();

            $loader = new \Twig\Loader\FilesystemLoader('app/View');
            $twig = new \Twig\Environment($loader);
            $template = $twig->load('categories.html');

            $parametros = array();
            $parametros['categorias'] = $categorias;
            //var_dump($colecPostagens);

            $conteudo = $template->render($parametros);
            echo $conteudo;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function create()
    {
        try {
            $loader = new \Twig\Loader\FilesystemLoader('app/View');
            $twig = new \Twig\Environment($loader);
            $template = $twig->load('addCategory.html');

            $conteudo = $template->render();
            echo $conteudo;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function store()
    {
        try {
            Categoria::create($_POST);

            echo '<script>alert("Categoria inserida com sucesso!");</script>';
            echo '<script>location.href="?pagina=categoria&metodo=index"</script>';
        } catch (Exception $e) {
            echo '<script>alert("' . $e->getMessage() . '");</script>';
            echo '<script>location.href="?pagina=categoria&metodo=create"</script>';
        }
    }

    public function edit($paramId)
    {
        try {
            $loader = new \Twig\Loader\FilesystemLoader('app/View');
            $twig = new \Twig\Environment($loader);
            $template = $twig->load('editCategory.html');

            $categoria = Categoria::loadById($paramId);

            $parametros = array();
            $parametros['id'] = $categoria->id;
            $parametros['nome'] = $categoria->nome;
            $parametros['codigo'] = $categoria->codigo;

            $conteudo = $template->render($parametros);
            echo $conteudo;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function update()
	{
		try {
			Categoria::update($_POST);

			echo '<script>alert("Categoria alterado com sucesso!");</script>';
			echo '<script>location.href="?pagina=categoria&metodo=index"</script>';
		} catch (Exception $e) {
			echo '<script>alert("' . $e->getMessage() . '");</script>';
			echo '<script>location.href="?pagina=categoria&metodo=edit&id='.$_POST['id'].'"</script>';
		}
	}

    public function destroy($paramId)
    {
        try {
            Categoria::delete($paramId);

            echo '<script>alert("Categoria deletada com sucesso!");</script>';
            echo '<script>location.href="?pagina=categoria&metodo=index"</script>';
        } catch (Exception $e) {
            echo '<script>alert("' . $e->getMessage() . '");</script>';
            echo '<script>location.href="?pagina=categoria&metodo=index"</script>';
        }
    }
}
